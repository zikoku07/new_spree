// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require chosen-jquery
//= require_tree .

$(function () {
    $('.custom-chosen').chosen({
        width: "100%"
    });

    $('.reset-search').click(function () {
        search_input = $(this).parent('form').find("input[type='text']");
        search_input.focus().val('');
        $(this).remove();
    });

    //$(".site-search-form input[type='text']").autocomplete({
    //    source: '/search_suggestion',
    //    minChars: 2,
    //    dataType: 'json'
    //});
});

function popupMessage(message, klass) {
    notificationTop = "+" + ($(document).scrollTop() + 60);
    $('#notification').removeClass().addClass('alert ' + klass);
    $('#flash-msg-text').html(message);
    $('#notification').show().animate({
        top: notificationTop
    }, 200);
    $('#notice-close').click(function () {
        $('#notification').hide();
        return true;
    });
    setTimeout(function () {
        $('#notification').hide().animate({
            top: "-60"
        }, 500);
    }, 4000);
}


$(document).ajaxStart(function () {
    startLoader();
});

$(document).ajaxComplete(function () {
    stopLoader();
});

function startLoader() {
    $('body').append("<div class='loading'> <i class='fa fa-spinner fa-spin'></i> </div>");
}

function stopLoader() {
    $(".loading").remove();
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
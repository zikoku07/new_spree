$(function(){

	mobileDevice =/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	// scrollbar js initialization
	$(".nano").nanoScroller();

	// Global Variables
	ww = $(window).width();



// Temprorary view of password Sign up page
var timeoutId = 0;
$('.temproray-view-password').on('mousedown', function() {
	setTimeout(function(){
		$('#password').attr('type','text')
	}, 10);
}).on('mouseup mouseleave', function() {
	$('#password').attr('type','password')
	clearTimeout(timeoutId);
});

// Dropdown menu Mobile

$('.toggle-icon').click(function(){
	// hiding other opened attributes if any
	$('.body-inactive-sm,.body-inactive-lg').trigger('click');

	// main function
	navigationElement = $('.menu-layer');
	findIfMenuAlreadyOpen = $('body').hasClass('pushed');
	if(findIfMenuAlreadyOpen){
		navigationElement.removeClass('show-menu-layer');
		$('body').removeClass('pushed');
		$('.body-inactive-sm').fadeOut(100);
	}
	else{
		navigationElement.addClass('show-menu-layer');
		$('body').addClass('pushed');
		$('.body-inactive-sm').fadeIn(100);
	}
});
// sliding Down main submenu on click for tab and mobile
$('.drop-down > a').click(function(e){
	if(ww<1024){
		ifDD = $(this).parent().children('.submenu').css('display');
		if(ifDD == 'block'){
			$(this).parent().children('.submenu').stop().slideUp(350);
			$(this).children('.glyphicon').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
		}
		else{
			$('.submenu').slideUp(0);
			$(this).parent().children('.submenu').slideDown(350);
			$('.drop-down > a i').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
			$(this).children('.glyphicon').addClass('glyphicon-menu-up').removeClass('glyphicon-menu-down');
		}
		e.preventDefault();
	}
});

// sliding Down the inner submenu
$('.es-title').click(function(){
	if(ww<1024){
		findDropState = $(this).next('ul').css('display');
		if(findDropState == 'block'){
			$(this).next('ul').stop().slideUp(350);
			$(this).children('.glyphicon').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
		}
		else{
			$('.submenu ul').slideUp(0);
			$(this).next('ul').stop().slideDown(350);
			$('.es-title i').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
			$(this).children('.glyphicon').addClass('glyphicon-menu-up').removeClass('glyphicon-menu-down');
		}
	}
})


// untoggle menu canvas
$('.clear-toggle').click(function(){
	$('.body-inactive-sm').fadeOut(100);
	$('.submenu').stop().slideUp(350);
	$('body').removeClass('pushed');
	$('.menu-layer').removeClass('show-menu-layer');
});

// Cart Dropdown
$('.cart-icon,.close-cart-dropdown').click(function(){
	// hiding other opened attributes if any
	if(ww<768){
		$('.body-inactive-sm,.body-inactive-lg').trigger('click');
	}

	// main function
	ifCartAlreadyOpen = $('.cart-dropdown').css('display');
	if(ifCartAlreadyOpen == 'none'){
		$('.cart-dropdown').slideDown(350);
		$('.body-inactive-lg').fadeIn(100);
	}
	else{
		$('.cart-dropdown').slideUp(350);
		$('.body-inactive-lg').fadeOut(100);
	}


	manageCartHeight();
})

// Search Toggle in Mobile
$('.search-icon').click(function(){
	// hiding other opened attributes if any
	$('.body-inactive-sm,.body-inactive-lg').trigger('click');

	// main function
	ifSearchBoxAlreadyOpen = $('.search-box').css('display');
	if(ifSearchBoxAlreadyOpen == 'none'){
		$('.search-box').slideDown(350);
		$('.body-inactive-lg').fadeIn(100);
	}
	else{
		$('.search-box').slideUp(350);
		$('.body-inactive-lg').fadeOut(100);
	}
})

//Backdrop Clearances
$('.body-inactive-lg,.body-inactive-sm').click(function(){
	$(this).fadeOut(100);
	$('.submenu').stop().slideUp(350);
	$('body').removeClass('pushed');
	$('.menu-layer').removeClass('show-menu-layer');
	$('.cart-dropdown').slideUp(350);
	if(ww<1024){
		$('.search-box').slideUp(350);
	}
});

// Manage Cart Height
manageCartHeight = function(){
	hh = $(window).height();
	headerHeight = $('.header').height();
	cartListHeight = $('.cart-dropdown-list').height();
	if(cartListHeight > hh-headerHeight){
		$('.cart-dropdown-list').height(hh-headerHeight - 100);
	}
};

// Dropdown on Chunk Left

$('.chunk-left > h3').click(function(){
	findIfChunkAlreadyOpen = $(this).parent().children('ul').css('display');
	if(findIfChunkAlreadyOpen == 'none'){
		$(this).parent().children('ul').stop().slideDown(350);
		$(this).children('i').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
	}
	else{
		$(this).parent().children('ul').stop().slideUp(350);
		$(this).children('i').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
});

originalChunkText = $('.chunk-left > h3 > span').text(); 
activeChunkText = $('.chunk-left ul > li.active > a').text();
manaChunkTexts = function(){
	if(ww <=1023){
		$('.chunk-left > h3 > span').text(activeChunkText);
	}
	else{
		$('.chunk-left > h3 > span').text(originalChunkText);	
	}
}

manaChunkTexts();

// Manage menu hover desktop version
backDrop = $('<div class="backDrop"></div>').prependTo('body');

$('.drop-down').hover(

	function () {
		$('.backDrop').show().css({"top":$('.header').height()+"px"});
	}, 

	function () {
		$('.backDrop').hide();
	}
	);


// Category left flank dropdowns

$('.cat-left-title').click(function(){
	findUlState = $(this).parent().children('ul').css('display');

	if(ww<=768){
		if(findUlState == 'block'){
			$(this).parent().children('ul').stop().slideUp(350);
			$(this).children('i').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
			opened = 0;
		}
		else{
			$('.cat-left-menu ul').slideUp(350);
				$('.cat-left-title').children('i').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
			$(this).parent().children('ul').stop().slideDown(350);
			$(this).children('i').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
			opened = 1;
		}
	}
});

//Product Listing Page
// Custom Panel collapse arrow change
$('.customPanel.product-listing-panel .panel-title a').click(function () {
	ifAlreadyCollapsed = $(this).hasClass('collapsed');
	if(ifAlreadyCollapsed){
		$(this).children('span').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
	}
	else{
		$(this).children('span').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
});
// Horizontal variation Box Hover
$('.variations .item').hover(

	function () {
		findOriginalImage = $(this).closest('.product-item').find('> a > img').attr('src');
		findOnHoverImage = $(this).attr('data-hover-src');	
		$(this).closest('.product-item').find('> a > img').attr('src',findOnHoverImage);
		$(this).addClass('active');
	}, 

	function () {
		$(this).closest('.product-item').find('>a > img').attr('src',findOriginalImage);
		$(this).removeClass('active');
	}
	);

// Panel Dropdown Mobile
$('.filter-text').click(function(){
	findIfPanelAlreadyOpen = $(this).next('.product-listing-panel').css('display');
	if(findIfPanelAlreadyOpen == 'none'){
		$(this).next('.product-listing-panel').stop().slideDown(350);
		$(this).children('i').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
	}
	else{
		$(this).next('.product-listing-panel').stop().slideUp(350);
		$(this).children('i').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
});


//Product Detail Page

// Custom Panel collapse arrow change
$('.customPanel.product-detail-panel .panel-title a').click(function () {
	ifAlreadyCollapsed = $(this).hasClass('collapsed');
	if(ifAlreadyCollapsed){
		$('.panel-title a span').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
		$(this).children('span').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
	}
	else{
		$(this).children('span').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
});
// Vertical variation Box Hover
// on hover
$('.vertical-variations ul li').hover(

	function () {
		findOriginalImage = $('.product-lrg-image > img').attr('src');
		findOnHoverImage = $(this).attr('data-hover-src');
		$('.product-lrg-image > img').attr('src',findOnHoverImage);
		$(this).addClass('hoverActive');
	}, 

	function () {
		if(!$(this).hasClass('clickActive')){
			$('.product-lrg-image > img').attr('src',findOriginalImage);
			$(this).removeClass('hoverActive');
		}
	}
	);

// on click
$('.vertical-variations ul li').click(function(){
	findOnClickImage = $(this).attr('data-hover-src');	
	$('.vertical-variations ul li').removeClass('hoverActive clickActive');
	$(this).addClass('clickActive');
	$('.product-lrg-image > img').attr('src',findOnClickImage);
	$('.product-lrg-image > a').attr('href',findOnClickImage);
})

// checkbox selections
$('.checkboxes ul li').click(function(){
	$(this).parent('ul').children('li').removeClass('active');
	$(this).addClass('active');
});

// scroll to review section
$('.scroll-to-review-sec > a').click(function(e){
	findReviewSectionOffsetTop = $('.product-reviews').offset().top;
	$('body, html').animate({
		scrollTop: findReviewSectionOffsetTop - 20
	},500);
	e.preventDefault();
})


// Window Resizing and orientationchange call
$(window).bind('resize orientationchange', function(){
	ww = $(window).width();
	manaChunkTexts();
})

})
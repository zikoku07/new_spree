class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_cookies
  before_action :set_conversion_rate
  helper_method :static_shipment_method

  def set_cookies
    unless cookies[:country_code].present?
      response = load_country_info
      @country_code = response["country_code"]
      @country_name = response["country_name"]
      c = ISO3166::Country.find_country_by_name(@country_name)
      @currency_name = c.currency.name
      cookies[:currency] = c.currency.iso_code
      cookies[:country_code] = @country_code
      cookies[:country_name] = @country_name
    end
  end

  def static_shipment_method
    {
        'standard' => {tracking: 'Standard', cost: 0, shipped_at: Date.today + 6.days},
        'premium' => {tracking: 'Premium', cost: 9.0, shipped_at: Date.today + 3.days},
        'express' => {tracking: 'Express', cost: 19.00, shipped_at: Date.today + 2.days}
    }
  end

  def set_conversion_rate
    if cookies[:currency].present?
      to = cookies[:currency]
    else
      response = load_country_info
      c = ISO3166::Country.find_country_by_name(response['country_name'])
      to = c.currency.iso_code
      cookies[:currency] = to
    end

    @money_conversion_rate = cookies["rate_#{to}".to_sym]

    unless @money_conversion_rate.present?
      @money_conversion_rate = get_currency_rate(to)
      cookies["rate_#{to}".to_sym] = @money_conversion_rate
    end
    @money_conversion_rate = @money_conversion_rate.to_f
  end

  def load_country_info
    if Rails.env == "development"
      ip = '61.247.185.157'
    else
      ip = request.ip
    end
    url = "freegeoip.net/json/#{ip}"
    response = RestClient.get(url)
    JSON.parse(response)
  end

  def get_currency_rate(to)
    begin
      url = "https://www.google.com/finance/converter?a=1&from=USD&to=#{to}"
      response = RestClient.get(url)
      response = Nokogiri::HTML(response)
      return response.css('.bld').text.split(' ').first.to_f
    rescue => ex
      p "Error with load currency rate: #{ex.message}"
      return 1
    end
  end

end

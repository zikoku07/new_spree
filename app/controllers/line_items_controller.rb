class LineItemsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def update
    @line_item = Spree::LineItem.find_by_id(params[:id])
    order = @line_item.order
    if params[:quantity].present?
      add_or_remove_quantity = params[:quantity].to_i - @line_item.quantity
      if add_or_remove_quantity > 0
        @line_item = order.contents.add(@line_item.variant, add_or_remove_quantity, {})
      elsif add_or_remove_quantity < 0
        @line_item = order.contents.remove(@line_item.variant, add_or_remove_quantity.abs, {})
      end
      @line_item.save
    end
  end

  def destroy
    @line_item = Spree::LineItem.find_by_id(params[:id])
    @line_item.destroy
  end

  private

  def item_params
    params.permit
  end

end
Spree::Admin::BaseController.class_eval do
  def authorize_admin
    if respond_to?(:model_class, true) && model_class
      record = model_class
    else
      record = Object
    end
    authorize! :admin, record
    authorize! action, record
  end
end
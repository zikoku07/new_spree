module Spree
  class BrandController < Spree::StoreController
    def index
      @brands = Spree::Brand.select(:id, :name, :slug).all
      if params[:taxonomy].present?
        @taxonomy = Spree::Taxonomy.friendly.find(params[:taxonomy])
        p @taxonomy.inspect
        if @taxonomy.present?
          @brands = @brands.where(taxonomy_id: @taxonomy.id)
        end
      end
      @brands = @brands.order('lower(name) asc').group_by(&:name)
    end

    def show
      @brand = Spree::Brand.friendly.find(params[:id])
      @title = "Brandcruz - #{@brand.name}"
      @description = "Brandcruz.com - #{@brand.name}"
      @taxonomy = Spree::Taxonomy.find_by_name('BRANDS')
      page = params[:page] || 1
      @products = Spree::Product.joins(:brand).where("lower(spree_brands.name) = ?", @brand.name.downcase).where(master_product: nil).includes(:variants, master: [:prices, :images]).page(page).per(60)
    end
  end
end
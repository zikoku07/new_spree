require 'rest-client'
require 'json'
module Spree
  class HomeController < Spree::StoreController
    helper 'spree/products'
    respond_to :html

    def index
      @title = 'BrandCruz.com: Online Shoes, Clothing, Free Shipping and Returns'
      # @searcher = build_searcher(params.merge(include_images: true))
      # @products = @searcher.retrieve_products
      # @products = @products.includes(:possible_promotions) if @products.respond_to?(:includes)
      #@taxonomies = Spree::Taxonomy.includes(root: :children)
      #OrderMailer.confirm_email(11).deliver_now
    end

    def load_state
      @country = Spree::Country.find_by_id(params[:code])
    end

    def change_currency

    end

    def save_change_currency
      c = ISO3166::Country.new(params[:country][0])
      @country_name = c.name
      cookies[:currency] = params[:currency].upcase
      cookies[:country_code] = params[:country][0]
      cookies[:country_name] = @country_name
      redirect_to root_path
    end

    def back_to_us
      cookies[:currency] = 'USD'
      cookies[:country_code] = 'US'
      cookies[:country_name] = 'United States'
      redirect_to root_path
    end

    def get_currency_from_country
      c = ISO3166::Country.new(params[:country_code])
      @currency_name = c.currency.iso_code.downcase
    end

    # def search_suggestion
    #   taxons = Spree::Taxon.where("name like '%#{params['term']}%'").limit(7)
    #   suggestion = taxons.collect { |tax| {value: tax.name, label: tax.name} }
    #   render json: suggestion.to_json
    # end

  end
end

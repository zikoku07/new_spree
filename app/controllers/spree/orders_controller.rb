module Spree
  class OrdersController < Spree::StoreController
    before_action :check_authorization
    before_action :apply_coupon_code, only: :update
    rescue_from ActiveRecord::RecordNotFound, :with => :render_404
    helper 'spree/products', 'spree/orders'

    respond_to :html

    before_action :assign_order_with_lock, only: :update
    skip_before_action :verify_authenticity_token, only: [:populate]

    def show
      @order = Order.includes(line_items: [variant: [:option_values, :images, :product]], bill_address: :state, ship_address: :state).find_by_number!(params[:id])
    end

    def update
      if @order.contents.update_cart(order_params)
        respond_with(@order) do |format|
          format.html do
            if params.has_key?(:checkout)
              @order.next if @order.cart?
              if @order.state == 'address'
                redirect_to checkout_address_path(state: 'address')
              elsif @order.state == 'payment'
                redirect_to checkout_payment_path(state: 'payment')
              else
                redirect_to checkout_state_path(@order.checkout_steps.first)
              end
            else
              redirect_to cart_path
            end
          end
        end
      else
        respond_with(@order)
      end
    end

    # Shows the current incomplete order from the session
    def edit
      @order = current_order || Order.incomplete.
          includes(line_items: [variant: [:images, :option_values, :product]]).
          find_or_initialize_by(guest_token: cookies.signed[:guest_token])
      associate_user
    end

    # Adds a new item to the order (creating a new order if none already exists)
    def populate
      @error = nil
      order = current_order(create_order_if_necessary: true)
      variant = Spree::Variant.find(params[:variant_id])
      quantity = params[:quantity].to_i
      size = params[:size]
      options = params[:options] || {}

      # 2,147,483,647 is crazy. See issue #2695.
      if quantity.between?(1, 2_147_483_647)
        begin
          line_item = order.contents.add(variant, quantity, options)
          line_item.size = size
          line_item.save
        rescue ActiveRecord::RecordInvalid => e
          @error = e.record.errors.full_messages.join(", ")
        end
      else
        @error = Spree.t(:please_enter_reasonable_quantity)
      end

      respond_to do |format|
        format.html do
          if @error
            flash[:error] = @error
            redirect_back_or_default(spree.root_path)
          else
            respond_with(order) do |format|
              format.html { redirect_to cart_path }
            end
          end
        end
        format.js {}
      end
    end

    def shipped_track
      @orders = spree_current_user.orders.complete.order('completed_at desc')
      @order = Spree::Order.find_by_id(params[:id])
      @trackes = @order.order_tracks.order('created_at desc').group_by { |track| track.created_at.strftime('%A, %d %b') }
    end

    def empty
      if @order = current_order
        @order.empty!
      end

      redirect_to spree.cart_path
    end

    def accurate_title
      if @order && @order.completed?
        Spree.t(:order_number, :number => @order.number)
      else
        Spree.t(:shopping_cart)
      end
    end

    def check_authorization
      order = Spree::Order.find_by_number(params[:id]) || current_order

      if order
        authorize! :edit, order, cookies.signed[:guest_token]
      else
        authorize! :create, Spree::Order
      end
    end

    def apply_coupon
      @order = current_order
      @error = "Order not found"
      if @order.present? && params[:coupon_code].present?
        @order.coupon_code = params[:coupon_code]

        handler = PromotionHandler::Coupon.new(@order).apply

        if handler.error.present?
          @error = handler.error
        elsif handler.success
          @success = handler.success
        end
      else
        @error = "Order or coupon code not found"
      end

      respond_to do |format|
        format.js {}
      end
    end

    def remove_coupon
      @order = current_order
      @success = false
      if @order.present?
        order_promotions = @order.order_promotions.where(promotion_id: params[:id]).first
        adjustments = @order.adjustments
        if adjustments.length > 0
          adjustments.destroy_all
        end
        order_promotions.destroy
        # Spree::Adjustable::AdjustmentsUpdater.update(@order)
        @success = true
      end
    end

    private

    def apply_coupon_code
      assign_order_with_lock

      if params[:order] && params[:order][:coupon_code] && @order.present?

        @order.coupon_code = params[:order][:coupon_code]

        handler = PromotionHandler::Coupon.new(@order).apply

        if handler.error.present?
          flash[:error] = handler.error
          respond_with(@order) { |format| format.html { redirect_to spree.cart_path } } and return
        elsif handler.success
          flash[:success] = handler.success
        end
      end
    end

    def order_params
      if params[:order]
        params[:order].permit(*permitted_order_attributes)
      else
        {}
      end
    end

    def assign_order_with_lock
      @order = current_order(lock: true)
      unless @order
        flash[:error] = Spree.t(:order_not_found)
        redirect_to root_path and return
      end
    end
  end
end

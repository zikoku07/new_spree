module Spree
  class PublicController < Spree::StoreController
    def contact_us
      @title = "Huge Selection of Shoes, Clothes, Bags & More. 24/7 Customer Service at BrandCruz!"
      @keywords = "Women's Shoes, Sale, Men's Shoes, Log In, Shipping and Returns"
      @description = "Free shipping BOTH ways on online shoes, clothing, and more! 60-day return policy, over 1000 brands, 24/7 friendly customer service."
      if request.get?
        @contact = ContactUs.new
      else
        @contact = ContactUs.new(contact_us_params)
        if @contact.save
          flash[:success] = 'Your contact request has been successfully submitted'
          redirect_to spree.contact_us_path
        else
          flash[:success] = 'Please input required fields'
          render :contact_us
        end
      end
    end

    def about_us

    end

    def domestic
      @title = "BrandCruz: Designer Apparel, Shoes, Handbags, & Beauty FREE SHIPPING"
      @keywords = "free shipping, coupon code, brand, cloth, shoes, women shoes"
      @description = "Free Shipping & Free Returns at Brandcruz.com. Shop the latest styles from top designers including Michael Kors, Tory Burch, Burberry, Christian Louboutin."
    end

    def international
      @title = "Brandcruz: Search and find the latest in fashion | Shipping Worldwide"
      @keywords = "Shoes, Dress, New to Sale, Designers,"
      @description = "FREE RETURNS & FREE 3-DAY SHIPPING WORLDWIDE -Women’s, Men’s Dresses, Handbags, Shoes, Jeans, Tops and more."
    end

    def privacy_policy

    end

    def term_condition

    end

    def safe_shopping_guarantee
      @title = "Brandcruz.com – Shop securely for Shoes, clothing, accessories and more on sale!"
      @keywords = "Women's Clothing, Sandals, Jackets & Coats, Women's Shoes"
      @description = "Discounted shoes, clothing, accessories and more at Brandcruz.com! Shop for brands you love on sale. Score on the Style, Score on the Price."
    end

    def secure_shopping
      @title = "Brandcruz.com - Up to 50% off luxury fashion‎ |Shop Secure"
      @keywords = "Sale, Shoes, Designers, Dresses, Clothing, Tops , Bags, porter"
      @description = "Shop designer fashion online at Brandcruz.com. Designer clothes, designer shoes, designer bags and designer accessories from top designer brands: ..."
    end

    def coupon

    end

    def faq
      @title = "Shop Brandcruz.com  for the latest trends and the best deals | BrandCruz"
      @keywords = "Plus size, Men, Women’s sale, sale sale, sale, hello"
      @description = "Brandcruz.com is the authority on fashion & the go-to retailer for the latest trends, must-have styles & the hottest deals. Shop dresses, tops, tees, leggings & more."
    end

    def not_found
      render :status => 404
    end

    def internal_error

    end

    def unacceptable

    end

    private

    def contact_us_params
      params.require(:contact_us).permit!
    end
  end
end
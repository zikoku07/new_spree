module Spree
  class TaxonomiesController < Spree::StoreController
    respond_to :html

    def show
      @taxonomy = Spree::Taxonomy.friendly.find(params[:id])
      @title = @taxonomy.meta_title
      @featured_category = Spree::Taxon.where(taxonomy_id: @taxonomy.id, is_featured: true, featured_menu: @taxonomy.name).order(:featured_menu_sl)
    end

  end
end

module Spree
  class TaxonsController < Spree::StoreController
    rescue_from ActiveRecord::RecordNotFound, with: :render_404
    helper 'spree/products'

    respond_to :html

    def show
      @taxon = Taxon.includes(:sub_taxons).friendly.find(params[:id])
      return unless @taxon
      @title = @taxon.meta_title
      page = params[:page] || 1
      @products = @taxon.products.includes(:variants, master: [:prices, :images]).order(:created_at).page(page).per(60)
      @featured_category = Spree::Taxon.where(is_featured: true, featured_menu: @taxon.name, taxonomy_id: @taxon.taxonomy_id)
    end


    private

    def accurate_title
      @taxon.try(:seo_title) || super
    end
  end
end

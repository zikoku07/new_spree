module ApplicationHelper

  def get_image(product_id)
    variant = Spree::Variant.where(product_id: product_id, is_master: true).first
    if variant.present?
      image = Spree::Asset.where(viewable_id: variant.id).first.image_link
      if image.present?
        image
      else
        small_image(Spree::Product.find_by_id(product_id))
      end
    end
  end

  def get_zoom_image(product_id)
    variant = Spree::Variant.where(product_id: product_id, is_master: true).first
    if variant.present?
      image = Spree::Asset.where(viewable_id: variant.id).first
      if image.zoomable_image_link.present?
        image.zoomable_image_link
      else
        "#{image.image_link}?op_sharpen=1&fit=fit,1&$filterlrg$&wid=1200&hei=1467"
      end
    else

    end
  end

  def get_image_for_product(product, image)
    variant = Spree::Variant.where(product_id: product.id, is_master: true).first
    if variant.present?
      image = Spree::Asset.where(viewable_id: variant.id).first.image_link
      if image.present?
        image
      else
        small_image(Spree::Product.find_by_id(product.id))
      end
    else

    end
  end

  def get_variant_image(variant)
    image_url = 'assets/not_available.png'
    if variant.present?
      image_url = variant.images.present? ? variant.images.first.image_link : ''
    end
    image_tag(image_url)
  end

  def display_order_status(order)
    if order.payment_state == 'failed'
      link_to 'Payment Failed Retry', spree.checkout_payment_path(state: 'payment', failed_id: order.id), class: 'mark-text'
    elsif order.approved?
      'Approved'
    elsif order.state == 'complete'
      'Processing'
    else
      Spree.t("order_state.#{order.state}").titleize
    end
  end

  def display_shipment_state(order)
    if order.shipment_state.present?
      if order.shipment_state == 'shipped'
        link_to 'Shipped Track now', spree.shipped_track_path(order.id)
      else
        Spree::Order::ORDER_SHIPMENT_STATE[order.shipment_state.to_sym]
      end
    else
      'Processing'
    end
  end

  def get_small_variant_image(variant, size = 50)
    image_url = 'assets/not_available.png'
    if variant.present?
      image_url = variant.images.present? ? variant.images.first.image_link : ''
    end
    image_tag(image_url, style: "width: #{50}px")
  end

  def mini_image(variant, size = 50)
    image_url = 'assets/not_available.png'
    if variant.present?
      image_url = variant.images.present? ? variant.images.first.image_link : ''
    end
    image_tag(image_url, style: "width: #{50}px")
  end

  def order_total_amount(order)
    order.line_items.collect { |item| item.quantity * item.price }.sum
  end

  def order_total_amount_with_currency(order)
    amount = order.line_items.collect { |item| item.quantity * item.price }.sum
    price_with_currency(amount)
  end

  def get_varient_count(product_id)
    Spree::Variant.where(product_id: product_id).count
  end

  def get_product_brand(product_id)
    product = Spree::Product.find_by_id(product_id)
    if product.present?
      product.brand.present? ? product.brand.name : ''
    end
  end

  def get_product_name(product_id)
    Spree::Product.find_by_id(product_id).present? ? Spree::Product.find_by_id(product_id).name : ''
  end

  def get_price(product_id)
    variant = Spree::Variant.where(product_id: product_id, is_master: true).first
    if variant.present?
      price = Spree::Price.where(variant_id: variant.id).first
      if price.price_range.present?
        price.price_range
      elsif price.amount.present?
        "#{map_currency}#{(price.amount * money_conversion_rate).round(2)}"
      end
    end
  end

  def get_price_for_product(product_id)
    variant = Spree::Variant.where(product_id: product_id, is_master: true).first
    if variant.present?
      price = Spree::Price.where(variant_id: variant.id).first
      if price.price_range.present?
        # return price.price_range
        p = price.price_range.split('$').second
        if p.include? ","
          p = p.gsub! ",", ''
        end
        "#{map_currency}#{(p.split('-').first.to_f * money_conversion_rate).round(2)} - #{(p.split('-').last.to_f * money_conversion_rate).round(2)}"
      elsif price.amount.present?
        "#{map_currency}#{(price.amount * money_conversion_rate).round(2)}"
      end
    end
  end

  def product_price_without_currency(product_id)
    variant = Spree::Variant.where(product_id: product_id, is_master: true).first
    if variant.present?
      price = Spree::Price.where(variant_id: variant.id).first
      if price.price_range.present?
        p = price.price_range.split('$').second
        "#{(p.split('-').first.to_f).round(2)} - #{(p.split('-').last.to_f).round(2)}"
      else
        "#{(price.amount).round(2)}"
      end
    end
  end

  def get_orginal_price_for_product(product_id, text)
    variant = Spree::Variant.where(product_id: product_id, is_master: true).first
    if variant.present?
      price = Spree::Price.where(variant_id: variant.id).first
      if price.price_range.present?
        ''
      elsif price.original_price.present?
        "#{text}:  #{map_currency}#{(price.original_price * money_conversion_rate).round(2)}"
      end
    end
  end

  def price_with_currency(price)
    "#{map_currency}#{(price * money_conversion_rate).round(2)}"
  end

  def map_currency
    if cookies['currency'].present? && cookies['currency'].downcase == 'usd'
      '$'
    else
      "#{cookies['currency']} "
    end
  end

  def money_conversion_rate
    currency = cookies['currency']
    if @money_conversion_rate.present?
      @money_conversion_rate
    else
      cookies["rate_#{currency.upcase}"].to_f || 1
    end
  end

  def get_shop_url(shop)
    url = ''
    split = shop.name.split(' ')
    split.each do |sp|
      cat = Spree::Taxon.where(name: sp, taxonomy_id: shop.taxonomy_id).first
      if cat.present?
        url = category_url(cat)
      end
    end
    if url.present?
      return url
    else
      split.each do |sp|
        cat = Spree::Taxon.where("name LIKE ? AND taxonomy_id = ?", "%#{sp}%", shop.taxonomy_id).first
        if cat.present?
          return category_url(cat)
        end
      end
    end
  end

  def you_saved(order)
    saving = 20
    if order.present?
      order.line_items.each do |item|
        variant = item.variant
        if variant.prices.present?
          v_price = variant.prices.first
          if v_price.original_price.present? && v_price.selling_price.present?
            saving += (v_price.original_price - v_price.amount.to_f) * item.quantity
          end
        end
      end
    end
    price_with_currency(saving)
  end

  def get_taxnomy_id(name)
    taxnomy = Spree::Taxonomy.find_by_name(name)
    if taxnomy.present?
      taxnomy.slug
    else
      '#'
    end
  end

  def get_all_variant(product_id)
    Spree::Variant.where(product_id: product_id, is_master: false)
  end

  def get_additional_images(product)
    variant = Spree::Variant.where(product_id: product.id, is_master: true).first
    Spree::Asset.where(viewable_id: variant.id).order(:id)
  end

  def get_additional_images_for_product

  end

  def variant_assets(id)
    Spree::Asset.where(viewable_id: id, viewable_type: 'Spree::Variant')
  end

  def get_varient_price(variant_id)
    amount = Spree::Price.where(variant_id: variant_id).first.amount
    "#{cookies['currency']} #{(amount * money_conversion_rate).round(2)}"
  end

  def get_member_product(product)
    Spree::Product.where(id: product.member_id).first
  end

  def generate_sub_menu(top_menu, top_cat_name)
    html = ""
    top_cat = Spree::Taxon.joins(:taxonomy).where("lower(spree_taxonomies.name) = ? and lower(spree_taxons.name) = ? and parent_id IS NULL", top_menu.downcase, top_cat_name.downcase).first

    if top_cat.present?
      html += '<ul>'
      sub_menues = Spree::Taxon.where(parent_id: top_cat.id)
      sub_menues.each do |sub_menu|
        html += "<li> #{link_to sub_menu.name, category_url(sub_menu)} </li>"
      end
      html += '</ul>'
    end
    raw html
  end

  def category_url(taxon)
    return spree.categories_path(taxon.permalink)
  end

  def get_active_class(action, menu_action)
    action == menu_action ? 'active' : ''
  end

  def get_county_code
    if cookies[:country_code].present?
      return cookies[:country_code].downcase
    else
      # url = 'freegeoip.net/json/?callback'
      # response = RestClient.get(url)
      # response = JSON.parse(response)
      # @country_code = response["country_code"]
      # @country_name = response["country_name"]
      # c = ISO3166::Country.find_country_by_name(@country_name)
      # @currency_name = c.currency.name
      # cookies['currency'] = c.currency.iso_code
      # cookies[:country_code] = @country_code
      # cookies[:country_name] = @country_name
      # return @country_code.downcase
    end
  end

  def line_item_count
    current_order.present? ? current_order.line_items.count : 0
  end

  def custom_flash_messages(opts = {})
    ignore_types = ["order_completed"].concat(Array(opts[:ignore_types]).map(&:to_s) || [])

    flash.each do |msg_type, text|
      unless ignore_types.include?(msg_type)
        return render partial: 'spree/shared/flash_message', locals: {msg_type: msg_type, text: text}
      end
    end
    nil
  end

  def user_country
    if cookies[:country_code].present?
      spree_country = Spree::Country.find_by_iso(cookies[:country_code].upcase)
      spree_country.present? ? spree_country.id : ''
    end
  end

  def country_lists
    Spree::Country.all.order(:name)
  end

  def remove_maycs(text)
    text = text.gsub('\n', '')
    if text.include? "macys"
      text = text.gsub! 'macys', 'BrandCruz'
    end

    if text.include? "macy's"
      text = text.gsub! "macy's", 'BrandCruz'
    end

    if text.include? "macy&#039;s"
      text = text.gsub! "macy&#039;s", 'BrandCruz'
    end

    if text.include? "macy"
      text = text.gsub! 'macy', 'BrandCruz'
    end
    if text.include? "1-800-BUY-MACY"
      text = text.gsub! '1-800-BUY-MACY', 'BrandCruz'
    end
    if text.include? "1.800.BUY.MACYS"
      text = text.gsub! '1.800.BUY.MACYS', 'BrandCruz'
    end
    if text.include? "1.800.BUY.MACY"
      text = text.gsub! '1.800.BUY.MACY', 'BrandCruz'
    end
    if text.include? "MACY"
      text = text.gsub! 'MACY', 'BrandCruz'
    end
    if text.include? "Macy"
      text = text.gsub! 'Macy', 'BrandCruz'
    end

    return text
  end


  def product_bullets(p_bullets)
    html = ""
    begin
      bullets = eval(p_bullets)
      if bullets.kind_of?(Array)
        bullets.each do |bullet|
          html += "<li style='list-style: circle;'> #{remove_maycs(bullet)} </li>"
        end
      end
    rescue => ex
      return ''
    end
    raw html
  end

  def home_meta_tags
    meta_data = {
        keywords: 'Brandcruz, Brandcruz.com, clothing, apparel, shoes, clothing store, accessories, Brandcruz online store',
        description: "BrandCruz.com - Free Shipping, Every Day, Every Order. Best Value on Designer Women's Apparel, Men's Apparel, Shoes, Handbags, Beauty and More. Millions of Satisfied Customers!",
    }
    meta_data.map do |name, content|
      tag('meta', name: name, content: content)
    end.join("\n")
  end

  def brand_meta_tags
    meta_data = {
        keywords: '',
        description: @description,
    }
    meta_data.map do |name, content|
      tag('meta', name: name, content: content)
    end.join("\n")
  end

  def display_promo_code(order)
    html = ""
    if order.promotions.present?
      promo = order.promotions.last
      html += "<div class='alert alert-success' style='width: 98%;'> Promo code #{promo.code} applied to this order </div>"
      html += link_to remove_coupon_path(promo.id), class: 'remove-coupon', style: 'color: #c6000e;', remote: true do
        raw "<i class='fa fa-times'></i> Remove"
      end
      raw html
    end
  end

  def display_promo_discount(order)
    html = ""
    if order.promotions.present?
      html += "<span class='clearfix'>"
      html += "<span class='pull-left'> Discount / coupon </span>"
      html += "<span class='pull-right'> #{price_with_currency(order.adjustment_total)} </span>"
      html += "</span>"
      raw html
    end
  end

  def generate_meta_tags
    if controller_name == 'home'
      home_meta_tags
    elsif controller_name == 'brand'
      brand_meta_tags
    else
      custom_meta_data_tags
    end
  end

  def custom_meta_data_tags
    custom_meta_data = meta_data
    if @description.present?
      custom_meta_data = custom_meta_data.merge(description: @description)
    end

    if @keywords.present?
      custom_meta_data = custom_meta_data.merge(keywords: @keywords)
    end

    custom_meta_data.map do |name, content|
      tag('meta', name: name, content: content)
    end.join("\n")
  end

  def generate_brand_link(brands)
    html = ''
    if brands.kind_of?(Array) && brands[1].present?
      brand = brands[1].last
      if brand.present? && brand.name.present?
        html = "<li class='col-lg-2 col-md-3 col-sm-4 custom-column brand-list-item'>"
        html += link_to brand.name, "/b/#{brand.slug}"
        html += '</li>'
      end
    end
    raw html
  end

end

module Spree
  class OrderMailer < BaseMailer
    default "Message-ID" => "#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@brandcruz.com"
    default from: "Brandcruz.com <order@brandcruz.com>"
    helper ApplicationHelper

    def confirm_email(order, resend = false)
      @order = order.respond_to?(:id) ? order : Spree::Order.find(order)
      # subject = (resend ? "[#{Spree.t(:resend).upcase}] " : '')
      #subject += "#{Spree::Store.current.name} #{Spree.t('order_mailer.confirm_email.subject')} ##{@order.number}"
      subject = 'Your Brandcruz.com Order Confirmation'
      mail(to: [@order.email, 'mahabubziko@gmail.com'], subject: subject, delivery_method_options: Spree::Order::ORDER_SMTP) # TODO: Need to change the email to 'ae35048@gmail.com' before going live
    end

    def update_order(order)
      @order = order
      subject = 'Your Brandcruz.com order status has been updated'
      mail(to: @order.email, subject: subject, delivery_method_options: Spree::Order::ORDER_SMTP)
    end

    def cancel_email(order, resend = false)
      @order = order.respond_to?(:id) ? order : Spree::Order.find(order)
      subject = (resend ? "[#{Spree.t(:resend).upcase}] " : '')
      subject += "#{Spree::Store.current.name} #{Spree.t('order_mailer.cancel_email.subject')} ##{@order.number}"
      mail(to: @order.email, subject: subject, delivery_method_options: Spree::Order::ORDER_SMTP)
    end
  end
end

module Spree
  class UserMailer < BaseMailer
    default "Message-ID" => "#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@brandcruz.com"

    def welcome(user)
      @user = user
      mail(to: user.email, from: 'info@brandcruz.com', subject: 'Welcome To Brandcruz.com')
    end

    def reset_password_instructions(user, token, *args)
      @edit_password_reset_url = spree.edit_spree_user_password_url(:reset_password_token => token)
      @user = user
      mail to: user.email, from: 'info@brandcruz.com', subject: 'Brandcruz ' + I18n.t(:subject, :scope => [:devise, :mailer, :reset_password_instructions])
    end

  end
end
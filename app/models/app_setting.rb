class AppSetting
  INQUIRY_TYPE = ['Order Status', 'International order', 'Order Assistance', 'Career', 'Investors', 'Website Feedback']
  HOME_PAGE_FEATURED_LIST_CLASS = {
      "home" => 'col-sm-3',
      "bed-&-bath" => 'col-sm-4',
      "women" => 'col-sm-6',
      "men" => 'col-sm-2',
      "juniors" => 'col-sm-2',
      "kids" => 'col-lg-2 col-md-3 col-sm-4',
      "active" => 'col-sm-2',
      "beauty" => 'col-sm-2',
      "shoes" => 'col-sm-4',
      "handbags" => 'col-sm-2',
      "jewelry" => 'col-sm-4',
      "watches" => 'col-sm-4',
      "brands" => 'col-sm-3',
  }
end
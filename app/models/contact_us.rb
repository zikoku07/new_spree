class ContactUs < ActiveRecord::Base
  # after_create :send_email_notification

  def send_email_notification
    NotificationMailer.send_contact_notification(self).deliver_now
  end
end

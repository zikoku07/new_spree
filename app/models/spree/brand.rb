module Spree
  class Brand < ActiveRecord::Base
    extend FriendlyId
    friendly_id :slug_candidates, use: :slugged

    has_many :products, :class_name => 'Spree::Product'

    def slug_candidates
      [
          :name,
          [:name, :id],
      ]
    end

    def self.replace_macys
      mac_brands = self.where("lower(name) like '%macy%'")
      mac_brands.each do |br|
        if br.name.include? "Macy's"
          br.name = br.name.gsub("Macy's", "Brandcruz")
          br.slug = br.slug.present? ? br.slug.gsub("macy-s", "brandcruz") : 'brandcruz'
        else
          br.name = "Brandcruz"
          br.slug = "brandcruz"
        end
        br.save
        p "Info: ID: #{br.id} and url: #{br.slug}"
      end
    end

  end
end
module Spree
  class MemberProduct < ActiveRecord::Base
    belongs_to :product, class_name: 'Spree::Product'
    belongs_to :member, class_name: 'Spree::Product', foreign_key: :member_id
  end
end

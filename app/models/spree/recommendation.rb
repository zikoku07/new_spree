module Spree
  class Recommendation < ActiveRecord::Base
    belongs_to :product, class_name: 'Spree::Product'
    belongs_to :recommend_product, class_name: 'Spree::Product', foreign_key: :recommended_product_id

    def own_product
      Spree::Product.find_by_id(self.id)
    end
  end
end
module Spree
  class Size < ActiveRecord::Base
    belongs_to :product, :class_name => 'Spree::Product'
  end
end
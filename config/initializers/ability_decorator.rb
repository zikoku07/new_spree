class AbilityDecorator
  include CanCan::Ability
  def initialize(user)
    if user.respond_to?(:has_spree_role?) && user.has_spree_role?('sub_admin')
      can [:admin, :index, :show, :edit, :track, :update_state], Spree::Order
      can [:admin, :index, :show, :edit, :update, :orders, :addresses, :items], Spree::User
    end
  end
end

Spree::Ability.register_ability(AbilityDecorator)
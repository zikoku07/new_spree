Rails.application.routes.draw do

  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
  mount Spree::Core::Engine, at: '/'
  # namespace 'spree' do
  #   resources :taxonomies, only: [:show]
  # end

  get 'change_currency' => 'spree/home#change_currency'
  get 'save_change_currency' => 'spree/home#save_change_currency'
  get 'back_to_us' => 'spree/home#back_to_us'
  get 'get_currency_from_country' => 'spree/home#get_currency_from_country'

  resources :line_items, only: [:update, :destroy]
  resources :stores, only: [:edit_address]

  get 'load_state/:code', to: 'spree/home#load_state', as: :load_state

  get '/about_us', to: 'spree/public#about_us', as: :about_us
  get '/privacy_policy', to: 'spree/public#privacy_policy', as: :privacy_policy
  get '/term_condition', to: 'spree/public#term_condition', as: :term_condition
  get '/coupon', to: 'spree/public#coupon', as: :coupon

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  get '/404', to: 'spree/public#not_found'
  get '/422', to: 'spree/public#unacceptable'
  get '/500', to: 'spree/public#internal_error'
end

Spree::Core::Engine.routes.draw do
  get '/c/*id', to: 'taxons#show', as: :categories
  get '/m/*id', to: 'taxonomies#show', as: :menu
  get '/p/*id', to: 'products#show', as: :brandcruz_product
  get '/brands', to: 'brand#index', as: :brandcruz_brands
  get '/brands/*taxonomy', to: 'brand#index', as: :brandcruz_taxonomy_brands
  get '/b/*id', to: 'brand#show', as: :brandcruz_brand
  get '/product/search', to: 'products#keyword_search', as: :product_search
  get '/free-domestic-shipping-both-ways', to: 'public#domestic', as: :domestic
  match '/dedicated-customer-support', to: 'public#contact_us', via: [:get, :post], as: :contact_us
  get '/free-shipping-worldwide', to: 'public#international', as: :international
  get '/safe_shopping_guarantee', to: 'public#safe_shopping_guarantee', as: :safe_shopping_guarantee
  get '/secure_shopping', to: 'public#secure_shopping', as: :secure_shopping
  get '/forget_password', to: 'public#forget_password', as: :forget_password
  get '/welcome', to: 'public#welcome', as: :welcome
  get '/search_suggestion', to: 'home#search_suggestion', as: :search_suggestion
  get '/order/:id/shipped_track_now', to: 'orders#shipped_track', as: :shipped_track
  get '/admin/orders/:id/tracks', to: 'admin/orders#track', as: :admin_order_track
  get '/brand-cruz-frequently-asked-question', to: 'public#faq', as: :faq
  get '/checkout-sign-up', to: 'checkout#registration', as: :checkout_signup
  get '/secure-checkout-flow-cgn-shipping-address', to: 'checkout#edit', as: :checkout_address
  get '/secure-payment-flow-cng-capture', to: 'checkout#edit', as: :checkout_payment
  post '/apply_coupon', to: 'orders#apply_coupon', as: :apply_coupon
  get '/remove_coupon/:id', to: 'orders#remove_coupon', as: :remove_coupon
  get '/404', to: 'public#not_found'
  get '/422', to: 'public#unacceptable'
  get '/500', to: 'public#internal_error'
  get '/show_cc/:id', to: 'admin/payments#show_cc', as: :show_cc
  patch '/admin/order/:id/update_state/', to: 'admin/orders#update_state', as: :order_state

  devise_scope :spree_user do
    get '/secure-user-signin-signup', to: 'user_sessions#new', as: :user_session
  end
end
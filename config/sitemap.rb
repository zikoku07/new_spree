# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.brandcruz.com/"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:

  #Basic public link

  add '/secure-user-signin-signup', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/change_currency', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/back_to_us', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/about_us', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/privacy_policy', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/term_condition', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/coupon', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/free-domestic-shipping-both-ways', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/dedicated-customer-support', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/free-shipping-worldwide', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/safe_shopping_guarantee', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/secure_shopping', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/brand-cruz-frequently-asked-question', :priority => 1, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/checkout-sign-up', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/secure-checkout-flow-cgn-shipping-address', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/secure-payment-flow-cng-capture', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/product/search', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now
  add '/brands', :priority => 0.5, :changefreq => 'weekly', :lastmod => DateTime.now

  # Generate all top menu

  Spree::Taxonomy.all.each do |taxonomy|
    add "/m/#{taxonomy.slug}", :priority => 0.75, :lastmod => DateTime.now, :changefreq => 'weekly'
  end

  Spree::Taxon.where("parent_id IS NOT NULL OR is_featured = true").each do |taxon|
    add "/c/#{taxon.permalink}", :priority => 0.75, :lastmod => DateTime.now, :changefreq => 'weekly'
  end

  Spree::Product.all.each do |product|
    add "/p/#{product.slug}", :priority => 0.75, :lastmod => DateTime.now, :changefreq => 'daily'
  end

  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end

class CreateSizes < ActiveRecord::Migration
  def change
    create_table :spree_sizes do |t|
      t.integer :product_id
      t.string :text
      t.string :short_name

      t.timestamps null: false
    end
  end
end

class CreateBrands < ActiveRecord::Migration
  def change
    create_table :spree_brands do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end

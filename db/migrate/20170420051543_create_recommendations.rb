class CreateRecommendations < ActiveRecord::Migration
  def change
    create_table :spree_recommendations do |t|
      t.integer :product_id
      t.integer :recommended_product_id

      t.timestamps null: false
    end
  end
end

class AddCategoryIdFieldToSpreeProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :taxon_id, :integer
    add_column :spree_products, :brand_id, :integer
  end
end

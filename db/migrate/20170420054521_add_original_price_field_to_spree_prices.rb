class AddOriginalPriceFieldToSpreePrices < ActiveRecord::Migration
  def change
    add_column :spree_prices, :original_price, :float
  end
end

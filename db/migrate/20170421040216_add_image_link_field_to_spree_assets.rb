class AddImageLinkFieldToSpreeAssets < ActiveRecord::Migration
  def change
    add_column :spree_assets, :image_link, :string
  end
end

class AddIsMasterFieldToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :is_master, :boolean, default: false
    add_column :spree_products, :master_product, :integer
  end
end

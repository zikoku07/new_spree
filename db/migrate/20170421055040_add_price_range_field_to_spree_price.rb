class AddPriceRangeFieldToSpreePrice < ActiveRecord::Migration
  def change
    add_column :spree_prices, :price_range, :string
  end
end

class AddIsFeaturedFieldToSpreeBrands < ActiveRecord::Migration
  def change
    add_column :spree_brands, :featured_image, :string
    add_column :spree_brands, :is_featured, :boolean, default: false
    add_column :spree_taxons, :featured_image, :string
    add_column :spree_taxons, :is_featured, :boolean, default: false
  end
end

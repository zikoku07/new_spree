class AddColorFieldToSpreeVariants < ActiveRecord::Migration
  def change
    add_column :spree_variants, :color, :string
    add_column :spree_variants, :color_image, :string
  end
end

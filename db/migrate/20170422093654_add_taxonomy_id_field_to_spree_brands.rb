class AddTaxonomyIdFieldToSpreeBrands < ActiveRecord::Migration
  def change
    add_column :spree_brands, :taxonomy_id, :integer
    add_column :spree_brands, :taxon_id, :integer
  end
end

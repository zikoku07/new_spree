class AddSerialNoFieldToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :serial_no, :integer,:unsigned => true, :null => false, :auto_increment => true, default:  1
  end
end

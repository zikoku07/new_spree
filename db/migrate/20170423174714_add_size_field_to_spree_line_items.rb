class AddSizeFieldToSpreeLineItems < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :size, :string
  end
end

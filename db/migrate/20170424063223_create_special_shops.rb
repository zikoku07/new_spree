class CreateSpecialShops < ActiveRecord::Migration
  def change
    create_table :special_shops do |t|
      t.integer :taxonomy_id
      t.string :image
      t.string :name

      t.timestamps null: false
    end
  end
end

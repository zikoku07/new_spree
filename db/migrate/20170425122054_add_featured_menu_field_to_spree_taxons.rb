class AddFeaturedMenuFieldToSpreeTaxons < ActiveRecord::Migration
  def change
    add_column :spree_taxons, :featured_menu, :string
    add_column :spree_taxons, :featured_menu_sl, :integer,:unsigned => true, :null => false, :auto_increment => true, default:  1
  end
end

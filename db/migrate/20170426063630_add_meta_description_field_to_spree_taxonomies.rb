class AddMetaDescriptionFieldToSpreeTaxonomies < ActiveRecord::Migration
  def change
    add_column :spree_taxonomies, :meta_description, :string
    add_column :spree_taxonomies, :meta_key, :string
    add_column :spree_taxonomies, :meta_title, :string
  end
end

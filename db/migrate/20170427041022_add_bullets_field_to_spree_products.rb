class AddBulletsFieldToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :bullets, :text
  end
end

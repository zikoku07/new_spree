class AddSellingPriceFieldToSpreePrice < ActiveRecord::Migration
  def change
    add_column :spree_prices, :selling_price, :float
  end
end

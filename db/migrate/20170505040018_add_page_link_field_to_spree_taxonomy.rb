class AddPageLinkFieldToSpreeTaxonomy < ActiveRecord::Migration
  def change
    add_column :spree_taxonomies, :page_link, :string
    add_column :spree_taxons, :page_link, :string
  end
end

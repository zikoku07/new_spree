class AddSlugToTaxonomies < ActiveRecord::Migration
  def change
    add_column :spree_taxonomies, :slug, :string
  end
end

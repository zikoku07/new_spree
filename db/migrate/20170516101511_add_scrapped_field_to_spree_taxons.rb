class AddScrappedFieldToSpreeTaxons < ActiveRecord::Migration
  def change
    add_column :spree_taxons, :scrapped, :boolean, default: false
  end
end

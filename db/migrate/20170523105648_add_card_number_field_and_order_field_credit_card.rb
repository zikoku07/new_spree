class AddCardNumberFieldAndOrderFieldCreditCard < ActiveRecord::Migration
  def change
    add_column :spree_credit_cards, :card_number, :text
    add_column :spree_credit_cards, :order_id, :integer
    add_column :spree_credit_cards, :secret_key, :text
  end
end

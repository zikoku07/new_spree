class CreateSpreeMemberProducts < ActiveRecord::Migration
  def change
    create_table :spree_member_products do |t|
      t.integer :product_id
      t.integer :member_id
      t.timestamps null: false
    end
  end
end

class AddSlugToBrands < ActiveRecord::Migration
  def change
    add_column :spree_brands, :slug, :string
  end
end

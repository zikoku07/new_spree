class AddSizechartFieldToSpreeProducts < ActiveRecord::Migration
  def change
    add_column :spree_products, :sizechart, :text
  end
end

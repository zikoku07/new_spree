class AddZoomableImageLinkToSpreeAssets < ActiveRecord::Migration
  def change
    add_column :spree_assets, :zoomable_image_link, :string
  end
end

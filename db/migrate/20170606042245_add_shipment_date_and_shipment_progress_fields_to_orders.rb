class AddShipmentDateAndShipmentProgressFieldsToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :shipment_date, :date
    add_column :spree_orders, :shipment_progress, :integer
  end
end

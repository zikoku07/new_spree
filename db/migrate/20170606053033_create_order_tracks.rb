class CreateOrderTracks < ActiveRecord::Migration
  def change
    create_table :spree_order_tracks do |t|
      t.text :comment
      t.integer :user_id
      t.integer :order_id

      t.timestamps null: false
    end
  end
end

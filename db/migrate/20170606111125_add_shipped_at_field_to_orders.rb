class AddShippedAtFieldToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :shipped_at, :datetime
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Spree::Core::Engine.load_seed if defined?(Spree::Core)
# Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

# Spree::Product.all.each_with_index do |val, ind|
#   val.serial_no =  ind + 1
#   val.save
# end

# taxon = Spree::Taxon.where("taxonomy_id = ? AND parent_id IS NOT NULL", 1)
#
# taxon.each do |tx|
#   p "#{tx.name}----------- id #{tx.id}"
# end

# price = Spree::Price.where("amount IS NULL")
#
# price.each do |pri|
#   p pri.inspect
# end

# p Spree::Taxon.find_by_id(60)



# sub_categories = Spree::Taxon.where("taxonomy_id = ?  AND parent_id IS NOT NULL", 1)
# sub_categories.each do |id|
#   p "ID------------------#{id.id} Name----------------------------#{id.name}"
# end
#
# taxonomy =  Spree::Taxonomy.find_by_name('ACTIVE')
#
# taxons = Spree::Taxon.where(taxonomy_id: taxonomy.id)
#
# taxons.each do |tx|
#   p "#{tx.id }------------------#{tx.name}"
# end


# Spree::Taxonomy.all.each do |tx|
#   tx.slug = tx.name.downcase.gsub(' ', '-')
#   tx.save
# end

# Spree::StockLocation.create(name: 'default')
#
# taxons = Spree::Taxon.where('lower(name) = ? AND is_featured = ?' , 'TOASTERS & TOASTER OVENS'.downcase,true)
#
#
# taxons.each do |tx|
#   p tx.id
#   p tx.taxonomy.name
# end
#
# prd = Spree::Classification.where(taxon_id: taxons.ids).count
#
# p "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
# p prd
# p "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"

# taxonomy = Spree::Taxonomy.find_by_name('HOME')
#
# taxon =  Spree::Taxon.where("taxonomy_id = ? AND parent_id IS NOT NULL", taxonomy.id).order(:id)
#
# taxon.each do |tax|
#   p tax.id
#   p tax.name
# end
#
# p taxon.count

taxons = Spree::Taxon.where(is_featured: true)

taxons.each do |tx|
  taxon = Spree::Taxon.where('lower(name) = ? AND parent_id IS NOT NULL' , tx.name.downcase).last
  if taxon.present?
    tx.permalink = taxon.permalink
    tx.scrapped = true
    tx.save
  end
end


taxo = Spree::Taxon.where("parent_id IS NOT NULL")

taxo.each do |tax|
  tax.scrapped = true
  tax.save
end




namespace :app_setting do
  desc 'Generate country and state list'
  task :generate_country_state_list => :environment do
    Spree::Country.delete_all
    Spree::State.delete_all
    Carmen::Country.all.each do |country|
      spree_country = Spree::Country.new
      spree_country.name = country.name
      spree_country.iso_name = country.name
      spree_country.iso = country.code
      spree_country.states_required = true
      p "country #{country.name}"
      if spree_country.save
        states = country.subregions
        if states.present?
          states.each do |state|
            spree_state = spree_country.states.build(name: state.name, abbr: state.code)
            spree_state.save
          end
        else
          spree_country.update_attributes(states_required: false)
        end
        p 'country added'
      else
        p 'error store country'
      end
    end
  end

  task :get_member_products => :environment do
    Scraper::get_member_products
  end

  task :remove_macys_from_brand => :environment do
    Spree::Brand.replace_macys
  end

  task :remove_macys_from_product => :environment do
    Spree::Product.replace_macys
  end

  task :update_fancy_product_url => :environment do
    Spree::Product.update_fancy_url
  end

end